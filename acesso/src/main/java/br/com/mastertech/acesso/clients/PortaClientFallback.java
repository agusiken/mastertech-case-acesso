package br.com.mastertech.acesso.clients;


import br.com.mastertech.acesso.model.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta findById(Long id) {
        return null;
    }

}
