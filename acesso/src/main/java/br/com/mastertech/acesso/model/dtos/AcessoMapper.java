package br.com.mastertech.acesso.model.dtos;

import br.com.mastertech.acesso.model.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso toAcesso(AcessoDTO acessoDTO) {
        Acesso acesso = new Acesso();
        acesso.setClienteId(acessoDTO.getCliente_id());
        acesso.setPortaId(acessoDTO.getPorta_id());
        return acesso;
    }

    public AcessoResponse toAcessoResponse(Acesso acesso) {
        AcessoResponse acessoResponse = new AcessoResponse();
        acessoResponse.setCliente_id(acesso.getClienteId());
        acessoResponse.setPorta_id(acesso.getPortaId());
        return acessoResponse;
    }

    public Acesso toAcesso(AcessoResponse acessoResponse) {
        Acesso acesso = new Acesso();
        acesso.setClienteId(acessoResponse.getCliente_id());
        acesso.setPortaId(acessoResponse.getPorta_id());
        return acesso;
    }


    public AcessoDTO toAcessoRequest(Acesso acesso) {
        AcessoDTO acessoDTO = new AcessoDTO();
        acessoDTO.setCliente_id(acesso.getClienteId());
        acessoDTO.setPorta_id(acesso.getPortaId());
        return acessoDTO;
    }


}
