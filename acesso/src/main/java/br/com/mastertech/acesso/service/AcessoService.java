package br.com.mastertech.acesso.service;

import br.com.mastertech.acesso.clients.ClienteClient;
import br.com.mastertech.acesso.clients.PortaClient;
import br.com.mastertech.acesso.exception.InvalidAcessoException;
import br.com.mastertech.acesso.model.Acesso;
import br.com.mastertech.acesso.model.Cliente;
import br.com.mastertech.acesso.model.Porta;
import br.com.mastertech.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Acesso criarAcesso(Acesso acesso) {
        Cliente cliente = findClienteById(acesso.getClienteId());
        Porta porta = findPortaById(acesso.getPortaId());

        acesso.setClienteId(cliente.getId());
        acesso.setPortaId(porta.getId());

        return acessoRepository.save(acesso);
    }

    public Acesso buscarAcesso(Acesso acesso) {
        Cliente cliente = findClienteById(acesso.getClienteId());
        Porta porta = findPortaById(acesso.getPortaId());

        Acesso acesseoRetorno = acessoRepository.findByClienteIdAndPortaId(acesso.getClienteId(), acesso.getPortaId());

        if (acesseoRetorno == null) {
            throw new InvalidAcessoException();
        }
        return acesseoRetorno;
    }

    public Cliente findClienteById(Long id) {
        return clienteClient.findById(id);
    }

    public Porta findPortaById(Long id) {
        return portaClient.findById(id);
    }


}
