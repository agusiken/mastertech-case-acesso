package br.com.mastertech.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta invalida")
public class PortaInvalidException extends RuntimeException {
}
