package br.com.mastertech.acesso.model.dtos;

public class AcessoResponse {

    public Long porta_id;

    public Long cliente_id;

    public Long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(Long porta_id) {
        this.porta_id = porta_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }
}
