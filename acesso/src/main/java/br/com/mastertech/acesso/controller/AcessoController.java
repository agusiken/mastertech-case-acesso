package br.com.mastertech.acesso.controller;

import br.com.mastertech.acesso.model.Acesso;
import br.com.mastertech.acesso.model.dtos.AcessoDTO;
import br.com.mastertech.acesso.model.dtos.AcessoMapper;
import br.com.mastertech.acesso.model.dtos.AcessoResponse;
import br.com.mastertech.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    public ResponseEntity<?> criarAcesso(@RequestBody AcessoDTO acessoDTO) {

        AcessoMapper acessoMapper = new AcessoMapper();
        Acesso acesso = acessoMapper.toAcesso(acessoDTO);

        AcessoResponse acessoResponse = acessoMapper.toAcessoResponse(acessoService.criarAcesso(acesso));

        return ResponseEntity.status(201).body(acessoResponse);

    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public ResponseEntity<?> buscarAcessoPorIds(@PathVariable(name = "cliente_id") Long cliente_id,
                                                @PathVariable(name = "porta_id") Long porta_id) {
        AcessoMapper acessoMapper = new AcessoMapper();
        Acesso acesso = new Acesso();
        acesso.setClienteId(cliente_id);
        acesso.setPortaId(porta_id);

        AcessoResponse acessoResponse = acessoMapper.toAcessoResponse(acessoService.buscarAcesso(acesso));
        return ResponseEntity.status(200).body(acessoResponse);
    }
}