package br.com.mastertech.acesso.repository;

import br.com.mastertech.acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {
    Acesso findByClienteIdAndPortaId(Long clienteId, Long portaId);
}