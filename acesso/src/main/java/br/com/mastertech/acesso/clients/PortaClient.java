package br.com.mastertech.acesso.clients;

import br.com.mastertech.acesso.model.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "portaacesso", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/{id}")
    Porta findById(@PathVariable Long id);
}
