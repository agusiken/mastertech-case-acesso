package br.com.mastertech.cliente.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente inexistente")
public class ClienteNotFoundException extends RuntimeException{
}
