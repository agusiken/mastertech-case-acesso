package br.com.mastertech.cliente.service;

import br.com.mastertech.cliente.exception.ClienteNotFoundException;
import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.repository.ClienteRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Cliente findClienteById(Long id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(!clienteOptional.isPresent()){
            throw new ClienteNotFoundException();
        }

        return clienteOptional.get();
    }
}
