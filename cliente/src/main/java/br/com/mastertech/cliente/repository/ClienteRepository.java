package br.com.mastertech.cliente.repository;

import br.com.mastertech.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
