package br.com.mastertech.porta.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta inexistente")
public class PortaNotFoundException extends RuntimeException {
}
