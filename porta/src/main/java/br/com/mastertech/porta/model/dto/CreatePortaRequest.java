package br.com.mastertech.porta.model.dto;

public class CreatePortaRequest {

    private String andar;

    private String sala;

    public CreatePortaRequest(String andar, String sala) {
        this.andar = andar;
        this.sala = sala;
    }

    public CreatePortaRequest() {
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
