package br.com.mastertech.porta.model.dto;

import br.com.mastertech.porta.model.Porta;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {

    public Porta toPorta(CreatePortaRequest createPortaRequest) {
        Porta porta = new Porta();
        porta.setAndar(createPortaRequest.getAndar());
        porta.setSala(createPortaRequest.getSala());
        return porta;
    }

}
