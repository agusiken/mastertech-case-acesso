package br.com.mastertech.porta.repository;

import br.com.mastertech.porta.model.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {
}

