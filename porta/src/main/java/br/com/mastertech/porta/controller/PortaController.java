package br.com.mastertech.porta.controller;

import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.model.dto.CreatePortaRequest;
import br.com.mastertech.porta.model.dto.PortaMapper;
import br.com.mastertech.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @Autowired
    private PortaMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody CreatePortaRequest createPortaRequest) {
        Porta cliente = mapper.toPorta(createPortaRequest);
        cliente = portaService.create(cliente);

        return cliente;
    }

    @GetMapping("/{id}")
    public Porta getById(@PathVariable String id){
        return portaService.getById(Long.valueOf(id));
    }

}
